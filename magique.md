---
layout: page
title: Magique
permalink: /magique/
---

<div class="w3-center" markdown="1">
 <h1>❤️ ☀️ 🌴</h1>
</div>

<div class="w3-center" markdown="1">
J'AI VU UNE FLEUR SAUVAGE

QUAND J'AI SU SON NOM

JE L'AI TROUVÉ PLUS BELLE.

-> Hubert Reeves
</div>


**Pampa Bocca** en bref.

🌵 Jardin, Minimalisme et Permaculture

[<i class="fab fa-instagram"></i> _Instagram_]({{ site.instagram_page }}){:target="_blank"}

📅 2 Janvier 2018

📍 CANNES La Bocca

😇 Macha DA COSTA

### Histoire

A départ, il s'agissait de prendre en photo les semis et les plantes du jardin de la _casa_. L'idée était d'avoir un historique simple des divers végétaux (date, photo, phrase de description). Par exemple, cela permet de comparer les dates de floraisons du jasmin d'une année à l'autre.

Chez **Pampa Bocca**, chaque plante est unique et porte un nom. Par exemple, l'arbre à monnaie de la maison, dont le nom latin est _pachira aquatica_, s'appelle _Pachira_. D'ailleurs, chez **Pampa Bocca**, elle fait partie d'une grande famille avec :

+ _Yucca_
+ _Aloe Verae_
+ _Citronnier_
+ _Arbre de Jade_
+ _Ficus Ginseng_
+ _Anthurium_
+ _Jasmin_
+ _Orchidée_
+ _Crassula Columella_
+ _Menthe_
+ etc.

Enfin, la famille est si grande et certaines plantes n'ont pas encore de nom. En effet, il peut parfois être difficile de trouver le nom de plantes poussant, de manière anarchique, quelques part entre des cailloux et une bordure.

#### Fleur magique
<input type="button" value="Fais moi vibrer !" onclick="doVibrate();" />
<input type="button" value="Fais moi plus vibrer !" onclick="doVibrateMelody();" />

##### Géo Trouvetou
<input type="button" value="Où suis-je ?" onclick="geoFindMe();" />
<p id="status"></p>

##### Photogénie
<input type="button" value="Me vois-tu ?" onclick="startVideo();" />
<input type="button" value="Suis-je belle ?" onclick="takePicture();" />
<input type="button" value="Arrête !" onclick="stopVideo();" />
<video id="video" width="200" height="200" autoplay></video>
<canvas id="canvas" width="640" height="480"></canvas>

##### Mimosa Sensitive
<canvas id="canvas-touch" width="200" height="200" style="border:solid black 1px;">
  Pas possible
</canvas>
<p id="touch"></p>
