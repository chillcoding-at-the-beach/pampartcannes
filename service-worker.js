'use strict';

const CACHE_NAME = 'pampa-cache-v2';

const FILES_TO_CACHE = [
  './',
  './index.html',
  './favicon.ico',
  './manifest.json',
  './assets/css/w3.css',
  './assets/css/main.css',
  './assets/css/materialize.min.css',
  './assets/fa/css/brands.min.css',
  './assets/fa/webfonts/fa-brands-400.woff2',
  './assets/img/icons/icon-144x144.png',
  './assets/img/geranium.jpg',
  './assets/img/geranium.webp',
  './assets/img/hoya-kerrii.jpg',
  './assets/img/hoya-kerrii.webp',
  './assets/img/crassula-columella.jpg',
  './assets/img/crassula-columella.webp',
  './assets/img/anthurium.jpg',
  './assets/img/anthurium.webp',
];

self.addEventListener('install', (evt) => {
  console.log('[ServiceWorker] Install');
  // CODELAB: Precache static resources here.
  evt.waitUntil(
    caches.open(CACHE_NAME).then((cache) => {
      console.log('[ServiceWorker] Pre-caching offline page');
      return cache.addAll(FILES_TO_CACHE);
    })
  );

  self.skipWaiting();
});

self.addEventListener('activate', (evt) => {
  console.log('[ServiceWorker] Activate');
  // CODELAB: Remove previous cached data from disk.
  evt.waitUntil(
    caches.keys().then((keyList) => {
      return Promise.all(keyList.map((key) => {
        if (key !== CACHE_NAME) {
          console.log('[ServiceWorker] Removing old cache', key);
          return caches.delete(key);
        }
      }));
    })
  );
  self.clients.claim();
});

self.addEventListener('fetch', (evt) => {
  console.log('[ServiceWorker] Fetch', evt.request.url);
  evt.respondWith(
    caches.open(CACHE_NAME).then((cache) => {
      return cache.match(evt.request)
      .then((response) => {
        return response || fetch(evt.request);
      });
    })
  );
});
