---
layout: post
title:  "Crassula Columella"
date:   2019-12-18
categories: jekyll update
img: /assets/img/crassula-columella
---

La crassula columella est une petite plante grasse, formée de coussinets disposés
en pagode le long de la tige.

### Entretiens
☀️🌤️ Attention à l'effet loupe des fenêtres

🌡️ > -2°

💧 Arrosage toutes les 2 / 3 semaines

⛏️ Terre bien drainée

### Famille
Crassulaceae

### Origine
Afrique du Sud
