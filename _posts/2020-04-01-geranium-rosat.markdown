---
layout: post
title:  "Géranium Rosat"
date:   2017-08-30
categories: jekyll update
img: /assets/img/geranium
---

Le Géranium Rosat possède des fleurs de couleur rose à violette, elles éclosent
dès le printemps. Il a la particularité d'avoir des feuilles odorantes.

### Entretiens
☀️🌤️

🌡️ > 0°

💧 De temps en temps

⛏️ Terre normal

### Vertus
-> Soin de la peau

-> Rhumatisme

-> Problèmes circulatoires

-> Antibactérienne

-> Antifongique

-> Éloigne les insectes (moustiques)

### Genre
Pelargonium

### Origine
Afrique du Sud

### Histoire
Importé d'Afrique du Sud en 1600, il est cultivé à la Réunion, à Grasse, pour
son huile essentielle. Au XIX siècle, Grasse lance sa culture commerciale de Pélargonium
à odeur de rose, forte de son expérience commencée au XVI siècle.

### Usage
Parfumerie, Aromathérapie

### Composition
Sulfure de diméthyle

### Culture
Huile essentielle obtenue par distillation traditionnelle à la vapeur,
dans des alambics de cuivre / inox.

### Fabrication
400Kg. de feuilles,
250L. d'eau

### Références
 Wikipédia
